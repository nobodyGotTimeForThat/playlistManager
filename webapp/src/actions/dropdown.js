export const addDropdown = (dropdownId, dropdownType) => ({
  type: "ADD_DROPDOWN",
  id: dropdownId,
  dropdownType: dropdownType,
  isOpen: false
});

export const toggleDropdown = (dropdownId, dropdownType) => ({
  type: "TOGGLE_DROPDOWN",
  id: dropdownId,
  dropdownType: dropdownType
});

export const closeDropdowns = () => ({
  type: "CLOSE_DROPDOWNS"
});
