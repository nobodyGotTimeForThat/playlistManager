import uuid from "uuidv4";

export const addPlaylist = name => ({
  type: "ADD_PLAYLIST",
  id: uuid(),
  name: name
});

export const deletePlaylist = id => ({
  type: "DELETE_PLAYLIST",
  id: id
});

export const renamePlaylist = (id, newName) => ({
  type: "RENAME_PLAYLIST",
  id: id,
  newName: newName
});

export const selectPlaylist = id => ({
  type: "SELECT_PLAYLIST",
  id: id
});
