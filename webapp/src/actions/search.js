export const SET_FILTER = "SET_FILTER";
export const setFilter = filter => ({
  type: SET_FILTER,
  filter: filter
});

export const REQUEST_RESULTS = "REQUEST_RESULTS";
export const requestResults = filter => ({
  type: REQUEST_RESULTS,
  filter: filter
});

export const RECEIVE_RESULTS = "RECEIVE_RESULTS";
export const receiveResults = (filter, json) => ({
  type: RECEIVE_RESULTS,
  filter: filter,
  results: json.results
});
