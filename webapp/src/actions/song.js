export const addSongToStore = (id, title, artist, genre, year) => ({
  type: "ADD_SONG_TO_STORE",
  id: id,
  title: title,
  artist: artist,
  genre: genre,
  year: year
});

export const addSongToPlaylist = (songId, playlistId) => ({
  type: "ADD_SONG_TO_PLAYLIST",
  songId: songId,
  playlistId: playlistId
});

export const removeSongFromPlaylist = (songId, playlistId) => ({
  type: "REMOVE_SONG_FROM_PLAYLIST",
  songId: songId,
  playlistId: playlistId
});
