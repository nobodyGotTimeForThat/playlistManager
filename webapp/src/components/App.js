import React from "react";
import PlaylistList from "../containers/Playlist/VisiblePlaylist";
import SongList from "../containers/Song/GetSongsFromPlaylist";
import SongResults from "../containers/Search/SongSearchResults";
import SongFilter from "../containers/Search/SongFilter";
import { Container, Row, Col } from "reactstrap";

const App = ({ view }) => {
  return (
    <Container>
      <Row>
        <SongFilter />
      </Row>
      <Row>
        <Col xs="6">
          <PlaylistList />
        </Col>
        <Col xs="auto">
          {view === "filter" ? <SongResults /> : <SongList />}
        </Col>
      </Row>
    </Container>
  );
};

export default App;
