import React from "react";
import PropTypes from "prop-types";
import SongList from "../Song/SongList";

const SongResults = ({ songs = [] }) => {
  return <SongList songs={songs} filter={true} />;
};

SongResults.propTypes = {
  songs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      artist: PropTypes.string.isRequired,
      genre: PropTypes.string,
      year: PropTypes.number
    }).isRequired
  )
};

export default SongResults;
