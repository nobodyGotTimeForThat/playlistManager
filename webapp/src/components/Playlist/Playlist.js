import React from "react";
import PropTypes from "prop-types";
import { ListGroupItem, Button } from "reactstrap";
import PlaylistMenu from "../../containers/Playlist/PlaylistMenuContainer";

const Playlist = ({ playlist = {}, onSelect }) => (
  <ListGroupItem>
    <Button onClick={onSelect}>{playlist.name}</Button>
    <PlaylistMenu playlistId={playlist.id} />
  </ListGroupItem>
);

Playlist.propTypes = {
  playlist: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  onSelect: PropTypes.func.isRequired
};

export default Playlist;
