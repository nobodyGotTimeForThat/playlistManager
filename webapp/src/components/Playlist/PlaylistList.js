import React from "react";
import PropTypes from "prop-types";
import Playlist from "./Playlist";
import { ListGroup } from "reactstrap";
import AddPlaylist from "../../containers/Playlist/AddPlaylist";

const PlaylistList = ({ playlists = [], onPlaylistSelect }) => {
  return (
    <div>
      <AddPlaylist />
      <ListGroup>
        {playlists.map(playlist => (
          <Playlist
            playlist={playlist}
            onSelect={onPlaylistSelect(playlist.id)}
          />
        ))}
      </ListGroup>
    </div>
  );
};

PlaylistList.propTypes = {
  playlists: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,

  onPlaylistSelect: PropTypes.func.isRequired
};

export default PlaylistList;
