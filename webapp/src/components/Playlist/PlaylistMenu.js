import React from "react";
import PropTypes from "prop-types";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
  Form,
  Input
} from "reactstrap";

const PlaylistMenu = ({ onCreate, onRename, onDelete, toggle, isOpen }) => {
  let input;

  if (isOpen === null || isOpen === undefined) {
    onCreate();
  }

  return (
    <ButtonDropdown isOpen={isOpen} toggle={toggle}>
      <DropdownToggle caret>...</DropdownToggle>
      <DropdownMenu>
        <DropdownItem>
          {/*
                Rename playlist section
                After submit rename action will be dispatched
            */}
          <Form
            onSubmit={event => {
              event.preventDefault();
              if (!input.value.trim()) {
                return;
              }
              onRename(input.value);
              input.value = "";
            }}
          >
            <Input ref={node => (input = node)} />
            <Button type="submit"> Rename </Button>
          </Form>
        </DropdownItem>
        <DropdownItem divider />
        <DropdownItem>
          <Button
            onClick={event => {
              if (
                window.confirm("Do you really want to delete this playlist?")
              ) {
                onDelete();
              }
            }}
          >
            Delete playlist
          </Button>
        </DropdownItem>
      </DropdownMenu>
    </ButtonDropdown>
  );
};

PlaylistMenu.propTypes = {
  onCreate: PropTypes.func.isRequired,
  onRename: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  toggle: PropTypes.func.isRequired
};

export default PlaylistMenu;
