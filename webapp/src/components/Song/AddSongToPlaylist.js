import React from "react";
import PropTypes from "prop-types";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import PlaylistList from "../Playlist/PlaylistList";

const AddSongToPlaylist = ({
  playlists = [],
  onAddClick,
  dropdownOpen,
  dropdownToggle
}) => {
  return (
    <ButtonDropdown
      direction="right"
      isOpen={dropdownOpen}
      toggle={dropdownToggle}
    >
      <DropdownToggle caret>Add to playlist</DropdownToggle>
      <DropdownMenu>
        <DropdownItem>
          <PlaylistList onPlaylistSelect={onAddClick} playlists={playlists} />
        </DropdownItem>
      </DropdownMenu>
    </ButtonDropdown>
  );
};

AddSongToPlaylist.propTypes = {
  playlists: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.number.isRequired
    }).isRequired
  ),
  onAddClick: PropTypes.func.isRequired,
  dropdownOpen: PropTypes.bool,
  dropdownToggle: PropTypes.func.isRequired
};

export default AddSongToPlaylist;
