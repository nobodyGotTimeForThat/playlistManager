import React from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";

const RemoveSongFromPlaylist = ({ onRemoveClick }) => {
  return <Button onClick={onRemoveClick}>Remove song from playlist</Button>;
};

RemoveSongFromPlaylist.propTypes = {
  onRemoveClick: PropTypes.func.isRequired
};

export default RemoveSongFromPlaylist;
