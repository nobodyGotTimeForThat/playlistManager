import React from "react";
import PropTypes from "prop-types";
import SongMenu from "../../containers/Song/SongMenuContainer";

const Song = ({ song = {}, selectedPlaylistId }) => {
  // check for emptiness of props
  const genre = song.genre ? song.genre : " ";
  const year = song.year ? song.year : " ";

  return (
    <tr>
      <td>{song.title}</td>
      <td>{song.artist}</td>
      <td>{genre}</td>
      <td>{year}</td>
      {/* songMenu for actions with songs goes here*/}
      <td>
        {" "}
        <SongMenu song={song} selectedPlaylistId={selectedPlaylistId} />" "}
      </td>
    </tr>
  );
};

Song.propTypes = {
  song: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.string.isRequired,
    genre: PropTypes.string,
    year: PropTypes.number
  }).isRequired,
  selectedPlaylistId: PropTypes.string
};

export default Song;
