import React from "react";
import PropTypes from "prop-types";
import Song from "./Song";
import { Table } from "reactstrap";

const SongList = ({ songs = [], selectedPlaylistId, filter = false }) => (
  <div>
    <h4>{filter === true ? "Results" : "Songs"}</h4>
    <Table>
      <thead>
        <tr>
          <th>Title</th>
          <th>Artist</th>
          <th>Genre</th>
          <th>Year</th>
          <th />
        </tr>
      </thead>

      {songs.map(song => (
        <Song song={song} selectedPlaylistId={selectedPlaylistId} />
      ))}
    </Table>
  </div>
);

SongList.propTypes = {
  songs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      artist: PropTypes.string.isRequired,
      genre: PropTypes.string,
      year: PropTypes.number
    }).isRequired
  ).isRequired,
  selectedPlaylistId: PropTypes.string,
  filter: PropTypes.bool
};

export default SongList;
