import React from "react";
import PropTypes from "prop-types";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import RemoveSongFromPlaylist from "./RemoveSongFromPlaylist";
import AddSongToPlaylist from "../../containers/Song/AddSongToPlaylistContainer";

const SongMenu = ({
  songId,
  selectedPlaylistId,
  onRemoveClick,
  onCreate,
  dropdownOpen,
  dropdownToggle
}) => {
  if (!dropdownOpen) {
    onCreate();
  }

  return (
    <ButtonDropdown isOpen={dropdownOpen} toggle={dropdownToggle}>
      <DropdownToggle carret>...</DropdownToggle>
      <DropdownMenu>
        {/* AddSongToPlaylist entry */}
        <DropdownItem>
          <AddSongToPlaylist songId={songId} />
        </DropdownItem>

        {/*
                        RemoveSongFromPlaylist entry
                        RemoveSongFromPlaylist will not be displayed if no playlist is selected (selectedPlaylistId = null)
                    */}
        {selectedPlaylistId ? (
          <DropdownItem>
            <RemoveSongFromPlaylist onRemoveClick={onRemoveClick} />
          </DropdownItem>
        ) : (
          ""
        )}
      </DropdownMenu>
    </ButtonDropdown>
  );
};

SongMenu.propTypes = {
  songId: PropTypes.number.isRequired,
  selectedPlaylistId: PropTypes.number,
  onRemoveClick: PropTypes.func.isRequired,
  dropdownOpen: PropTypes.bool,
  dropdownToggle: PropTypes.func.isRequired
};

export default SongMenu;
