import { connect } from "react-redux";
import App from "../components/App";

const mapStateToProps = state => ({
  view: state.appState.currentView
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
