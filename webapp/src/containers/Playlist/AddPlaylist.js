import React from "react";
import { connect } from "react-redux";
import { addPlaylistMiddleware } from "../../functions/middleware/playlist";

const AddPlaylist = ({ dispatch }) => {
  let input;

  return (
    <div>
      <form
        onSubmit={event => {
          event.preventDefault();
          if (!input.value.trim()) {
            return;
          }
          dispatch(addPlaylistMiddleware(input.value));
          input.value = "";
        }}
      >
        <input ref={node => (input = node)} />
        <button type="submit">Add playlist</button>
      </form>
    </div>
  );
};

export default connect()(AddPlaylist);
