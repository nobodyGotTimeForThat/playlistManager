import { connect } from "react-redux";
import PlaylistMenu from "../../components/Playlist/PlaylistMenu";
import { addDropdown, toggleDropdown } from "../../actions/dropdown";
import { getDropdownState } from "../../functions/dropdown";
import {
  deletePlaylistMiddleware,
  renamePlaylistMiddleware
} from "../../functions/middleware/playlist";

let dropdownType = "playlist";

const mapStateToProps = (state = [], ownProps) => ({
  isOpen: getDropdownState(state.dropdowns, ownProps.playlistId, dropdownType)
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onCreate: () => dispatch(addDropdown(ownProps.playlistId, dropdownType)),
  onRename: newName =>
    dispatch(renamePlaylistMiddleware(ownProps.playlistId, newName)),
  onDelete: () => dispatch(deletePlaylistMiddleware(ownProps.playlistId)),
  toggle: () => dispatch(toggleDropdown(ownProps.playlistId, dropdownType))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlaylistMenu);
