import { connect } from "react-redux";
import PlaylistList from "../../components/Playlist/PlaylistList";
import { getAllPlaylistLite } from "../../functions/playlist";
import { showPlaylist } from "../../functions/middleware/playlist";

const mapStateToProps = state => ({
  playlists: getAllPlaylistLite(state.data.playlists)
});

const mapDispatchToProps = dispatch => ({
  // event for playlist select
  onPlaylistSelect: playlistId => dispatch(showPlaylist(playlistId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlaylistList);
