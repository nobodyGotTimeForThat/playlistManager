import React from "react";
import { connect } from "react-redux";
import {
  localFilter,
  setFilterMiddleware
} from "../../functions/middleware/search";

const SongFilter = ({ dispatch }) => {
  let input;

  return (
    <div>
      <form
        onSubmit={event => {
          event.preventDefault();
          if (!input.value.trim()) {
            return;
          }
          dispatch(setFilterMiddleware(input.value));
          input.value = "";
        }}
      >
        <input ref={node => (input = node)} />
        <button>Search</button>
      </form>
    </div>
  );
};

export default connect()(SongFilter);
