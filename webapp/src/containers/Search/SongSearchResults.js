import { connect } from "react-redux";
import SongResults from "../../components/Filter/SongResults";
import { searchSongs } from "../../functions/search";

const mapStateToProps = state => ({
  songs: searchSongs(
    state.data.songs,
    state.search.filter,
    state.search.onlineResults
  )
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SongResults);
