import { connect } from "react-redux";
import AddSongToPlaylist from "../../components/Song/AddSongToPlaylist";
import { getAllPlaylistLite } from "../../functions/playlist";
import { getDropdownState } from "../../functions/dropdown";
import { addSongToPlaylist } from "../../actions/song";
import { toggleDropdown } from "../../actions/dropdown";
import { addSongToPlaylistMiddleware } from "../../functions/middleware/song";

let dropdownType = "addSongToPlaylist";

const mapStatetoProps = (state, ownProps) => ({
  playlists: getAllPlaylistLite(state.data.playlists),
  dropdownOpen: getDropdownState(
    state.dropdowns,
    ownProps.song.id,
    dropdownType
  )
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onAddClick: playlistId =>
    dispatch(addSongToPlaylistMiddleware(ownProps.song, playlistId)),
  dropdownToggle: () => dispatch(toggleDropdown(ownProps.song.id, dropdownType))
});

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(AddSongToPlaylist);
