import { connect } from "react-redux";
import SongList from "../../components/Song/SongList";
import { getSongsFromPlaylist } from "../../functions/songs";

const mapStateToProps = state => ({
  songs: getSongsFromPlaylist(
    state.data.songs,
    state.data.playlists,
    state.appState.selectedPlaylistId
  ),
  selectedPlaylistId: state.appState.selectedPlaylistId
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SongList);
