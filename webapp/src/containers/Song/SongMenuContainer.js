import { connect } from "react-redux";
import SongMenu from "../../components/Song/SongMenu";
import { getDropdownState } from "../../functions/dropdown";
import { addDropdown, toggleDropdown } from "../../actions/dropdown";
import { removeSongFromPlaylistMiddleware } from "../../functions/middleware/song";

let dropdownType = "song";

const mapStateToProps = (state, ownProps) => {
  return {
    songId: ownProps.songId,
    selectedPlaylist: state.appState.selectedPlaylistId,
    dropdownOpen: getDropdownState(
      state.dropdowns,
      ownProps.songId,
      dropdownType
    )
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onCreate: () => dispatch(addDropdown(ownProps.songId, dropdownType)),
  onRemoveClick: () =>
    dispatch(removeSongFromPlaylistMiddleware(ownProps.songId)),
  dropdownToggle: () => dispatch(toggleDropdown(ownProps.songId, dropdownType))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SongMenu);
