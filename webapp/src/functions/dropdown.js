export const getDropdownState = (dropdowns = [], dropdownId, dropdownType) => {
  let results = dropdowns.filter(dropdown => {
    return dropdown.id === dropdownId && dropdown.dropdownType === dropdownType;
  });

  let isOpen = null;

  results.map(result => (isOpen = result.isOpen));

  return isOpen;
};
