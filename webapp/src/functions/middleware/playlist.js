import {
  addPlaylist,
  deletePlaylist,
  renamePlaylist,
  selectPlaylist
} from "../../actions/playlist";
import { closeDropdowns } from "../../actions/dropdown";
import { setView } from "../../actions/view";
import { saveState } from "../dataOperations/localStorage";

let viewMode = "playlist";

export const showPlaylist = playlistId => {
  return dispatch => {
    return () => {
      dispatch(selectPlaylist(playlistId));
      dispatch(setView(viewMode));
    };
  };
};

export const addPlaylistMiddleware = name => {
  return (dispatch, getState) => {
    dispatch(addPlaylist(name));
    dispatch(closeDropdowns());
    saveState({
      data: getState().data
    });
  };
};

export const renamePlaylistMiddleware = (playlistId, newName) => {
  return (dispatch, getState) => {
    return () => {
      dispatch(closeDropdowns());
      dispatch(renamePlaylist(playlistId, newName));
      saveState({
        data: getState().data
      });
    };
  };
};

export const deletePlaylistMiddleware = playlistId => {
  return (dispatch, getState) => {
    dispatch(closeDropdowns());
    dispatch(deletePlaylist(playlistId));
    saveState({
      data: getState().data
    });
  };
};
