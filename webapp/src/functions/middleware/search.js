import {
  receiveResults,
  requestResults,
  setFilter
} from "../../actions/search";
import { setView } from "../../actions/view";

let viewMode = "filter";
export const searchURL = 'https://api.discogs.com/database/search';
export const token = 'psVwmWqBmCvZpBOuInMqcRLNwmORdMXGPQSSxAYn';

export const setFilterMiddleware = filter => {
  return dispatch => {
    dispatch(setFilter(filter));
    dispatch(setView(viewMode));
    dispatch(fetchSearchResults(filter));
  };
};

export const fetchSearchResults = filter => {
  return dispatch => {
    dispatch(requestResults(filter));

    // build url string for fetch
      let url = searchURL + '?q=' + filter + '&token=' + token;

    // call api to get data
    return fetch(
      url
    )
      .then(
        // on success
        // extract json from response
        response => response.json()
      )
      .then(json =>
        // dispatch action with data from api
        dispatch(receiveResults(filter, json))
      )
      .catch(error => {
        console.log("Not able to ge data. ", error);
      });
  };
};
