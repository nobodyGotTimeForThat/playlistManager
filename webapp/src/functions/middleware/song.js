import {
  addSongToPlaylist,
  addSongToStore,
  removeSongFromPlaylist
} from "../../actions/song";
import { closeDropdowns } from "../../actions/dropdown";
import { getSongById } from "../songs";
import { saveState } from "../dataOperations/localStorage";

export const removeSongFromPlaylistMiddleware = songId => {
  return (dispatch, getState) => {
    return () => {
      dispatch(
        removeSongFromPlaylist(songId, getState().appState.selectedPlaylistId)
      );
      dispatch(closeDropdowns());
      saveState({
        data: getState().data
      });
    };
  };
};

export const addSongToPlaylistMiddleware = (song, playlistId) => {
  return (dispatch, getState) => {
    return () => {
      if (!getSongById(song.id, getState.data.songs)) {
        dispatch(
          addSongToStore(
            song.id,
            song.title,
            song.artist,
            song.genre,
            song.year
          )
        );
      }
      dispatch(addSongToPlaylist(song.id, playlistId));
      dispatch(closeDropdowns());
      saveState({
        data: getState().data
      });
    };
  };
};
