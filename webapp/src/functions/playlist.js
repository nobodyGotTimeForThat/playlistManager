import { selectPlaylist } from "../actions/playlist";
import { setView } from "../actions/view";

// return array of objects. Object contains only id and name of playlist

export const getAllPlaylistLite = (playlists = []) => {
  // check for emptiness of playlists
  if (!playlists) {
    return [];
  }
  return playlists.map(playlist => {
    return {
      id: playlist.id,
      name: playlist.name
    };
  });
};
