import _ from "lodash";

export const searchSongs = (songs = [], filter = "", onlineResults = []) => {
  let localDataResults = filterLocalSongs(songs, filter);
  let onlineDataResults = parseOnlineSongs(onlineResults);

  let results = _.concat(localDataResults, onlineDataResults);

  return _.uniqBy(results, song => song.id);
};

export const filterLocalSongs = (songs, filter) => {
  let rawResults = songs.filter(song => {
    return song.title === filter || song.artist === filter;
  });
  return _.uniqBy(rawResults, result => result.id);
};

export const parseOnlineSongs = results => {
  let rawResults = results
    .filter(result => result.type === "master")
    .map(result => {
      let artistTITLE = _.split(result.title, "-", 30);

      return {
        id: result.id,
        title: artistTITLE[1],
        artist: artistTITLE[0],
        year: result.year,
        genre: result.genre
      };
    });

  return _.uniqBy(rawResults, result => result.id);
};
