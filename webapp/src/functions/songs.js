/*
* return array of Song objects based on array of songIds in playlist object
* selectedPlaylistId is used to find Playlist from playlists (array of Playlist objects)
*/
export const getSongsFromPlaylist = (songs, playlists, selectedPlaylistId) => {
  // when no playlist is selected
  if (!selectedPlaylistId) {
    return;
  }

  // get list of song Ids from selected playlist
  const songsIdList = playlists.filter(playlist => {
    return playlist.id === selectedPlaylistId;
  }).songs;

  //get and return song objects
  return songs.filter(song => {
    return songsIdList.includes(song.id);
  });
};

export const getSongById = (songId, songs) => {
  return songs.filter(song => {
    return song.id === songId;
  });
};
