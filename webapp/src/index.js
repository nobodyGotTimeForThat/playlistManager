import React from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import registerServiceWorker from "./registerServiceWorker";
import { composeWithDevTools } from "redux-devtools-extension";
import { loadState } from "./functions/dataOperations/localStorage";
import rootReducer from "./reducers/index";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./containers/AppContainer";
import Provider from "react-redux/es/components/Provider";

let loadedState = loadState();

// debugging
const composeEnhancers = composeWithDevTools({});
const store = createStore(
  rootReducer,
  loadedState,
  composeEnhancers(applyMiddleware(thunk))
);

// release
// const store = createStore(rootReducer, loadedState, applyMiddleware(thunk));

// 1st render
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
