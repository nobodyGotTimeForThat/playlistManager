const appState = (state = {}, action) => {
  switch (action.type) {
    case "SELECT_PLAYLIST": {
      return {
        ...state,
        selectedPlaylistId: action.id
      };
    }

    case "SET_VIEW": {
      return { ...state, currentView: action.view };
    }

    default: {
      return state;
    }
  }
};

export default appState;
