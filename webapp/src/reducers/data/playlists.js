const playlists = (state = [], action) => {
  switch (action.type) {
    // actions with playlist
    case "ADD_PLAYLIST": {
      return [
        ...state,
        {
          id: action.id,
          name: action.name,
          songs: []
        }
      ];
    }
    case "RENAME_PLAYLIST": {
      return state.map(playlist => {
        return playlist.id === action.id
          ? { ...playlist, name: action.newName }
          : playlist;
      });
    }
    case "DELETE_PLAYLIST": {
      return state.filter(playlist => {
        return playlist.id !== action.id;
      });
    }

    // actions with songs in playlist
    case "ADD_SONG_TO_PLAYLIST": {
      return [
        ...state,
        state
          .filter(playlist => {
            return playlist.id === action.playlistId;
          })
          .songs.push(action.songId)
      ];
    }
    case "REMOVE_SONG_FROM_PLAYLIST": {
      return state
        .filter(playlist => {
          return playlist.id === action.playlistId;
        })
        .songs.filter(songId => {
          return songId !== action.songId;
        });
    }

    default: {
      return state;
    }
  }
};

export default playlists;

// playlist reducer and songIds reducer needs to be combined
