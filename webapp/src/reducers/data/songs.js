const songs = (state = [], action) => {
  switch (action.type) {
    case "ADD_SONG_TO_STORE": {
      return [
        ...state,
        {
          id: action.id,
          title: action.title,
          artist: action.artist,
          genre: action.genre,
          year: action.year
        }
      ];
    }

    default: {
      return state;
    }
  }
};

export default songs;
