const dropdowns = (state = [], action) => {
  switch (action.type) {
    case "ADD_DROPDOWN": {
      return [
        ...state,
        {
          id: action.id,
          dropdownType: action.dropdownType,
          isOpen: false
        }
      ];
    }

    case "TOGGLE_DROPDOWN": {
      return state.map(
        dropdown =>
          dropdown.id === action.id &&
          dropdown.dropdownType === action.dropdownType
            ? { ...dropdown, isOpen: !dropdown.isOpen }
            : dropdown
      );
    }

    case "CLOSE_DROPDOWNS": {
      return state.map(dropdown => ({
        ...dropdown,
        isOpen: false
      }));
    }

    default: {
      return state;
    }
  }
};

export default dropdowns;
