import { combineReducers } from "redux";
import playlists from "./data/playlists";
import songs from "./data/songs";
import appState from "./appState";
import dropdowns from "./dropdowns";
import search from "./search";

export default combineReducers({
  data: combineReducers({
    playlists,
    songs
  }),
  appState: appState,
  dropdowns: dropdowns,
  search: search
});
