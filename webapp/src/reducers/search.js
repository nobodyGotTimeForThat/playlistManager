import {
  RECEIVE_RESULTS,
  REQUEST_RESULTS,
  SET_FILTER
} from "../actions/search";

const search = (
  state = {
    filter: "",
    isFetching: false,
    results: []
  },
  action
) => {
  switch (action.type) {
    case SET_FILTER: {
      return { ...state, filter: action.filter };
    }
    case REQUEST_RESULTS: {
      return { ...state, isFetching: true };
    }
    case RECEIVE_RESULTS: {
      return { ...state, isFetching: false, onlineResults: action.results };
    }

    default: {
      return state;
    }
  }
};

export default search;
